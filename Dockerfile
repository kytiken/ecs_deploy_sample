FROM ruby:2.6-alpine

ENV APP_ROOT /app
ENV RAILS_ENV production

WORKDIR $APP_ROOT

RUN apk update && apk add mariadb-dev redis nodejs alpine-sdk libxml2-dev libxslt-dev tzdata
COPY Gemfile $APP_ROOT
COPY Gemfile.lock $APP_ROOT

RUN bundle install --jobs 4

COPY . $APP_ROOT
RUN bundle exec rails assets:precompile SECRET_KEY_BASE=$(bundle exec rails secret)
