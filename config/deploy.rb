# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, "my-app"
set :repo_url, "git@gitlab.com:kytiken/ecs_deploy_sample.git"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

set :ecs_default_cluster, "pulumi-awsx-ecs-sample-woITsS6mgy-dev-c554ed6"
set :docker_registry_host_with_port, "135439977482.dkr.ecr.ap-northeast-1.amazonaws.com"
set :target_group_arn, 'arn:aws:elasticloadbalancing:ap-northeast-1:135439977482:targetgroup/ecs-sample-ab19b67/c0ff9d8049d1fac1'
set :ecs_region, %w(ap-northeast-1) # optional, if nil, use environment variable
set :ecs_service_role, "ecsServiceRole" # default: ecsServiceRole
set :ecs_deploy_wait_timeout, 600 # default: 300
set :ecs_wait_until_services_stable_max_attempts, 40 # optional
set :ecs_wait_until_services_stable_delay, 15 # optional
set :docker_repository, "#{fetch(:docker_registry_host_with_port)}/my-app"
set :ecs_region, "ap-northeast-1"
set :desired_api_count, 2

set :ecs_tasks, [
  {
    name: "#{fetch(:application)}-#{fetch(:stage)}",
    container_definitions: [
      {
        name: fetch(:application),
        image: "#{fetch(:docker_repository)}:latest",
        memory_reservation: 512,
        command: ['bundle', 'exec', 'rails', 's', '-b', '0.0.0.0', '-p', '80'],
        environment: [
          { name: 'RAILS_ENV', value: fetch(:stage) },
          { name: 'SECRET_KEY_BASE', value: ENV['SECRET_KEY_BASE'] },
          { name: 'DATABASE_URL', value: ENV['DATABASE_URL'] },
        ],
        mount_points: [
          # {
          #   source_volume: 'sockets_path',
          #   container_path: '/app/tmp/sockets',
          #   read_only: false
          # },
          # {
          #   source_volume: 'public_path',
          #   container_path: '/app/public',
          #   read_only: false
          # }
        ],
        port_mappings: [
          { container_port: 80, host_port: 0, protocol: 'tcp' }
        ],
        volumes_from: [],
        log_configuration: {
          log_driver: 'awslogs',
          options: {
            'awslogs-group' => "/aws/ecs/#{fetch(:application)}/#{fetch(:stage)}/rails",
            'awslogs-region' => fetch(:ecs_region)
          }
        }
      },
      # {
      #   name: 'nginx',
      #   image: "nginx",
      #   memory_reservation: 128,
      #   environment: [
      #   ],
      #   port_mappings: [
      #     { container_port: 80, host_port: 0, protocol: 'tcp' }
      #   ],
      #   volumes_from: [
      #     {
      #       source_container: fetch(:application),
      #       read_only: true
      #     }
      #   ],
      #   log_configuration: {Configure ECS ARN setting 
      #     log_driver: 'awslogs',
      #     options: {
      #       'awslogs-group' => "/aws/ecs/#{fetch(:application)}/#{fetch(:stage)}/nginx",
      #       'awslogs-region' => fetch(:ecs_region)
      #     }
      #   }
      # }
    ],
    volumes: [
      # { name: 'sockets_path', host: {} },
      # { name: 'public_path', host: {} }
    ]
  }
]

set :ecs_services, [
  {
    name: "#{fetch(:application)}-#{fetch(:stage)}",
    load_balancers: [
      {
        target_group_arn: fetch(:target_group_arn),
        container_port: 80,
        container_name: fetch(:application)
      }
    ],
    desired_count: fetch(:desired_api_count),
    deployment_configuration: { maximum_percent: 200, minimum_healthy_percent: 50 }
  }
]

# set :ecs_executions_before_deploy, -> do
#   # ecs_deployの結果からTaskDefを取得
#   rake = fetch(:ecs_registered_tasks)[fetch(:ecs_region)]["#{fetch(:application)}-#{fetch(:slug)}"]
#   raise 'Not registered new task' unless rake

#   [
#     {
#       cluster: fetch(:ecs_default_cluster),
#       region: fetch(:ecs_region),
#       task_definition: {
#         task_definition_name: "#{rake.family}:#{rake.revision}",
#         main_container_name: fetch(:application)
#       },
#       command: ['bin/rails hikatsuku:db:migrate[true]'],
#       timeout: 600
#     }
#   ]
# end

# set :ecs_executions_after_deploy, -> do
#   # ecs_deployの結果からTaskDefを取得
#   rake = fetch(:ecs_registered_tasks)[fetch(:ecs_region)]["#{fetch(:application)}-#{fetch(:slug)}"]
#   raise 'Not registered new task' unless rake

#   [
#     {
#       cluster: fetch(:ecs_default_cluster),
#       region: fetch(:ecs_region),
#       task_definition: {
#         task_definition_name: "#{rake.family}:#{rake.revision}",
#         main_container_name: 'cardloan-nginx'
#       },
#       command: ['rm -rf /var/cache/ngx_pagespeed/*'],
#       timeout: 600
#     }
#   ]
# end

